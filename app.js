const express = require('express');
const app = express();
var http = require("http");

const productRouters = require('./api/routes/products');

const bodyParser = require('body-parser');

app.use('/products', productRouters);

app.use((req, res, next) => {
    console.log("Request recieved");
    console.log(res);
    console.log(req.method);
    if (req.method === "GET") {
        console.log('get call');
        req.on('body', function(body) {
            console.log(body);
        });
    } else if (req.method === "POST") {
        console.log('post call');
        // req.on('data', function(data) {
        //     console.log(data);

        // });
    } else if (req.method === "PUT") {
        console.log('put call');
        // req.on('data', function(data) {
        //     console.log(data);

        // });
    }
});
// app.use((res, req, next) => {
//     const error = new Error('not found');
//     error.status = 404;

//     next(error);

// })
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});
module.exports = app;